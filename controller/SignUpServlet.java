package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.service.BranchService;
import bbs.service.DepartmentService;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//支社と部署情報をサーバーへ送る
		List<Branch> branches = new BranchService().select();
		request.setAttribute("branches", branches);
		List<Department> departments = new DepartmentService().select();
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		List<Branch> branches = new BranchService().select();
		request.setAttribute("branches", branches);
		List<Department> departments = new DepartmentService().select();
		request.setAttribute("departments", departments);

		User user = getUser(request);
		if (!isValid(user, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user); //バリデーション
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}
		new UserService().insert(user);
		response.sendRedirect("./management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setValitationPass(request.getParameter("valitationPass"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		return user;
	}

	private boolean isValid(User user, List<String> errorMessages) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String valitationPass = user.getValitationPass();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();
		//	int[] headOffice = {1,2};
		//	int branchOffice = {"営業部","技術部"};

		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if (6 > account.length()) {
			errorMessages.add("アカウントは6文字以上で入力してください");
		} else if (20 < account.length()) {
			errorMessages.add("アカウントは20文字以下で入力してください");

		} else if (!account.matches("^([a-zA-Z0-9]{6,20})$")) {
			errorMessages.add("アカウント名は半角英数字かつ6文字以上20字以下で入力してください");
		} else if (new UserService().checkDuplication(account)) {//データベースに登録アカウントがある
			errorMessages.add("アカウントが重複しています");
		}

		if (StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");
		} else if (6 > password.length()) {
			errorMessages.add("パスワードは6文字以上で入力してください");
		} else if (20 < password.length()) {
			errorMessages.add("パスワードは20文字以下で入力してください");

		} else if (!password.matches("^[a-zA-Z0-9!#$%&()*+,.:;=?@\\[\\]^_{}-]{6,20}+$")) {
			errorMessages.add("パスワードは半角英数字記号のみ、6文字以上20字以下で入力してください");
		}

		if (!password.equals(valitationPass)) {
			errorMessages.add("パスワードが一致しません");
		}

		if (StringUtils.isEmpty(name)) {
			errorMessages.add("名前を入力してください");
		} else if (name.matches("^[  |　]+$")) {
			errorMessages.add("名前を入力してください");
		} else if (10 < name.length()) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		if (StringUtils.isEmpty(Integer.toString(branchId))) {
			errorMessages.add("支社を入力してください");
		}

		if (StringUtils.isEmpty(Integer.toString(departmentId))) {
			errorMessages.add("部署名を入力してください");
		}

		if (branchId == 1) {//本社
			if (!(departmentId == 1 || departmentId == 2)) {//総務人事と情報管理以外
				errorMessages.add("支社と部署の組合せが不適切です");
			}
		} else {//支社
			if (!(departmentId == 3 || departmentId == 4)) {//営業と技術部以外
				errorMessages.add("支社と部署の組合せが不適切です");
			}
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
package bbs.beans;

import java.io.Serializable;
import java.util.Date;

public class Users implements Serializable{
	private final short  VALID = 0;
	private int id;
	private String account;
	private String name;
	private String password;
	private String branchName;
	private String departmentName;
	private short isStopOrValid = VALID;

	private Date createdDate;
	private Date updatedDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public short getIsStopOrValid() {
		return isStopOrValid;
	}
	public void setIsStopOrValid(short isStopOrValid) {
		this.isStopOrValid = isStopOrValid;
	}
	public short getVALID() {
		return VALID;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}

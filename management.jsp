<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <link href="./css/common.css" rel="stylesheet" type="text/css">
 <link href="./css/management.css" rel="stylesheet" type="text/css">
 <script type="text/javascript" src="./js/ishiyama_tomoki.js"></script>

<title>ユーザー管理画面</title>
</head>
	<body>
		<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
		    <div class="errorMessages">
		        <ul>
		            <c:forEach items="${errorMessages}" var="errorMessage">
		                <li><c:out value="${errorMessage}" />
		            </c:forEach>
		        </ul>
		    </div>
		    <c:remove var="errorMessages" scope="session" />
		</c:if>

			<div class="header">
				<c:if test="${ not empty loginUser }">
					<a href="./">ホーム</a>
					<a href="signup">ユーザー新規登録</a>
				</c:if>
			</div>
<div class="usersInfo">
			<c:forEach items="${usersInfo}" var="user">

					<div class="userInfo">
						<span class="account user">
							<c:out value="${user.account}" />
						</span>
						<span class="name user">
							<c:out value="${user.name}" />
						</span>
						<span class="branchName user">
							<c:out value="${user.branchName}" />
						</span>
						<span class="departmentName user">
							<c:out value="${user.departmentName}" />
						</span>
						<span class="isStopped user">
							<c:out value="${user.isStopped}" />
						</span>

						<form action="setting" method="get">
							<input type="hidden" name="userId"
								value="${user.id}">

							<a href="setting"><input type="submit" value="編集" class="button"></a>
						</form>

						<c:if test="${user.id != loginUser.id }">
						<form action="stop" method="post" onSubmit="return continueConfirm()">
							<input type="hidden" name="userId"
								value="${user.id}">
								<c:if test="${user.isStopped != 0}">
									<input type="submit" value="復活" class="button">
									<input type="hidden" name="isStopped"  value = 0>
								</c:if>
						</form>
						<form action="stop" method="post" onSubmit="return stopConfirm()">
							<input type="hidden" name="userId"
								value="${user.id}">
								<c:if test="${user.isStopped == 0}">
									<input type="hidden" name="isStopped" value = 1>
									<input type="submit" value="停止" class="button stop">
								</c:if>
						</form>
					</c:if>
					</div>

			</c:forEach>
			</div>
</div>

	</body>
</html>
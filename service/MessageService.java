package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import bbs.beans.Message;
import bbs.beans.UserMessage;
import bbs.dao.MessageDao;
import bbs.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {


		Connection connection = null;
		try {
			connection  = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String strStart, String strEnd, String refineCategory) {
		final int LIMIT_NUM = 1000;
		Connection connection = null;

		//絞込み機能
		//絞込みの開始時刻が空白
		if(strStart == "" || strStart == null) {
			strStart = "2020-06-01 00:00:00";
		}else {//入力されたとき
			strStart = strStart + " 00:00:00";
		}
		//Date型へ変換
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date start = null;
		try {
			start = dateFormat.parse(strStart);
		} catch (ParseException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}

		Date end = null;
		//絞込みの開始時刻が空白
		if(strEnd == "" || strEnd == null) {
			end =  new Date();
		} else {
			strEnd = strEnd + " 23:59:59";
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				end = sdf.parse(strEnd);
			} catch (ParseException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}
		}

		//空白で入力された場合は空白文字がきてるのでNULLのときのみでOK
		if (refineCategory == null) {
			refineCategory = "";
		}

		try {
			connection = getConnection();
			List<UserMessage> messages = new UserMessageDao().select(connection, start, end, refineCategory, LIMIT_NUM);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int deletedId) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, deletedId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
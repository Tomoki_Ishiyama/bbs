package bbs.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.User;

@WebFilter(urlPatterns = { "/setting", "/management", "/signup" })
public class ManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletResponse httpResponse = (HttpServletResponse) response;
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpSession session = httpRequest.getSession();
		User user = (User) session.getAttribute("loginUser");
		String errorMessage = "管理者のみ閲覧できるページです。リダイレクトします。";
		String servletPath = httpRequest.getServletPath();

		//管理者・・・本社ID=1で総務人事ID=1のアカウント
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		//エラーにしたいことを先にかく
		//ユーザーが管理者じゃない
		if (servletPath.equals("/setting") || servletPath.equals("/management") ||
				servletPath.equals("/signup")) {

			if (branchId != 1 || departmentId != 1) {
				session.setAttribute("errorMessages", errorMessage);
				httpResponse.sendRedirect("./");
			} else {
				chain.doFilter(request, response);
				//httpResponse.sendRedirect(servletPath);
			}
		}
	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}

}

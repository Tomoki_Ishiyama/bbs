package bbs.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.beans.UserComment;
import bbs.beans.UserMessage;
import bbs.service.CommentService;
import bbs.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String strStart = request.getParameter("start");//絞込み開始日
		String strEnd = request.getParameter("end");
		String refineCategory = request.getParameter("refineCategory");

		List<UserMessage> messages = new MessageService().select(strStart, strEnd, refineCategory);
		List<UserComment> comments = new CommentService().select();

		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("strStart", strStart);//絞込み開始日
		request.setAttribute("strEnd", strEnd);
		request.setAttribute("preRefineCategory", refineCategory);
		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
}


package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Message;
import bbs.beans.User;
import bbs.service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		Message message = getMessage(request);
		if (!isValid(message, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("message.jsp").forward(request, response);
			return;
		}

		User user = (User) session.getAttribute("loginUser");
		message.setUserId(user.getId());

		new MessageService().insert(message);
		response.sendRedirect("./");
	}

	private Message getMessage(HttpServletRequest request) throws IOException, ServletException {
		Message message = new Message();

		message.setTitle(request.getParameter("title"));
		message.setCategory(request.getParameter("category"));
		message.setText(request.getParameter("text"));

		return message;
	}

	private boolean isValid(Message message, List<String> errorMessages) {

		String title = message.getTitle();
		String category = message.getCategory();
		String text = message.getText();

		if (StringUtils.isEmpty(title)) {
			errorMessages.add("タイトルを入力してください");
		} else if (title.matches("^[  |　]+$")) {
			errorMessages.add("タイトルを入力してください");
		} else if (30 < title.length()) {
			errorMessages.add("タイトルは30文字以下で入力してください");
		}

		if (StringUtils.isEmpty(category)) {
			errorMessages.add("カテゴリを入力してください");
		} else if (category.matches("^[  |　]+$")) {
			errorMessages.add("カテゴリを入力してください");
		} else if (10 < category.length()) {
			errorMessages.add("カテゴリは10文字以下で入力してください");
		}

		if (StringUtils.isEmpty(text)) {
			errorMessages.add("投稿内容を入力してください");
		} else if (text.matches("^[  |　]+$")) {
			errorMessages.add("投稿内容を入力してください");
		} else if (1000 < text.length()) {
			errorMessages.add("投稿内容は1000文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
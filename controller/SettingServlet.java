
package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.service.BranchService;
import bbs.service.DepartmentService;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {
	private static int edittingUserId;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		//userIdはhiddenでmanagement画面から送られてきている
		String strEdittingUserId = request.getParameter("userId");//IDをmanagementから受け取り

		boolean isError = true;

		if (strEdittingUserId == null || strEdittingUserId == "") { //直打ちされたとき
			isError = false;
		} else if (!strEdittingUserId.matches("^[0-9]+$")) {
			isError = false;
		}

		edittingUserId = Integer.parseInt(strEdittingUserId);
		User edittingUser = new UserService().select(edittingUserId);

		if (edittingUser == null || isError == false) {
			HttpSession session = request.getSession();
			String errorMessage = "不正なパラメータが入力されました";
			session.setAttribute("errorMessages", errorMessage);
			response.sendRedirect("./management");
			return;
		}

		//本社総務人事部のアカウントは変更できないようにする

		if (loginUser.getId() == edittingUserId) {
			List<Branch> branch = new ArrayList<Branch>();
			List<Department> department = new ArrayList<Department>();
			branch.add(branches.get(0));
			department.add(departments.get(0));
			branches = branch;
			departments = department;
		}

		//支社と部署情報をJSPへ送る
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.setAttribute("edittingUser", edittingUser);
		request.getRequestDispatcher("setting.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		//ユーザーをセッターに登録
		User user = getUser(request);
		User edittingUser = new UserService().select(edittingUserId);

		//本社総務人事部のアカウントは変更できないようにする
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		if (loginUser.getId() == edittingUserId) {
			List<Branch> branch = new ArrayList<Branch>();
			List<Department> department = new ArrayList<Department>();
			branch.add(branches.get(0));
			department.add(departments.get(0));
			branches = branch;
			departments = department;
		}
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);

		//パスワードが更新されていなかったらパスワード以外を更新
		try {
			boolean isInputPass;
			if (isInputPass = StringUtils.isEmpty(user.getPassword())) { //入力されたパスワードが空の時は
				if (isValid(user, edittingUser, errorMessages, isInputPass)) {
					new UserService().updateExceptPass(user);//パスワード以外を更新する
				}
			} else {
				if (isValid(user, edittingUser, errorMessages, isInputPass)) {
					new UserService().update(user);
				}
			}

		} catch (NoRowsUpdatedRuntimeException e) {
			errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
		}

		if (errorMessages.size() != 0) {

			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user);
			request.setAttribute("edittingUser", user);//edittingUser -> user
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		response.sendRedirect("./management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setValitationPass(request.getParameter("valitationPass"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		return user;
	}

	private boolean isValid(User user, User edittingUser, List<String> errorMessages, boolean flag) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String valitationPass = user.getValitationPass();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if (6 > account.length()) {
			errorMessages.add("アカウントは6文字以上で入力してください");
		} else if (20 < account.length()) {
			errorMessages.add("アカウントは20文字以下で入力してください");
		} else if (!account.matches("^([a-zA-Z0-9]{6,20})$")) {
			errorMessages.add("アカウント名は半角英数字かつ6文字以上20字以下で入力してください");
		} else if ((!edittingUser.getAccount().equals(user.getAccount())) && //アカウント入力欄にあらたに入力されたとき かつ
				new UserService().checkDuplication(account)) {//データベースに入力されたアカウントがあるとき
			errorMessages.add("アカウントが重複しています");
		}

		if (flag == false) {//パスワードが入力されているとき
			if (6 > password.length()) {
				errorMessages.add("パスワードは6文字以上で入力してください");
			} else if (20 < password.length()) {
				errorMessages.add("パスワードは20文字以下で入力してください");

			} else if (!password.matches("^[a-zA-Z0-9!#$%&()*+,.:;=?@\\[\\]^_{}-]{6,20}+$")) {
				errorMessages.add("パスワードは半角英数字記号のみ、6文字以上20字以下で入力してください");
			}

			if (!password.equals(valitationPass)) {
				errorMessages.add("パスワードが一致しません");
			}
		}

		if (StringUtils.isEmpty(name)) {
			errorMessages.add("名前を入力してください");
		} else if (name.matches("^[  |　]+$")) {
			errorMessages.add("名前を入力してください");
		} else if (10 < name.length()) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		if (StringUtils.isEmpty(Integer.toString(branchId))) {
			errorMessages.add("支社を入力してください");
		}

		if (StringUtils.isEmpty(Integer.toString(departmentId))) {
			errorMessages.add("部署名を入力してください");
		}

		if (branchId == 1) {//本社
			if (!(departmentId == 1 || departmentId == 2)) {//総務人事と情報管理以外
				errorMessages.add("支社と部署の組合せが不適切です");
			}
		} else {//支社
			if (!(departmentId == 3 || departmentId == 4)) {//営業と技術部以外
				errorMessages.add("支社と部署の組合せが不適切です");
			}
		}

		if (errorMessages.size() != 0) {
			return false;
		}

		return true;
	}


}
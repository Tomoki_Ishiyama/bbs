package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.User;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		request.getRequestDispatcher("login.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		String account = request.getParameter("account");
		String password = request.getParameter("password");

		//ユーザー情報をDBから取得
		User user = new UserService().select(account, password);

		//入力されたアカウントとパスが入力されてるかユーザーがいるかを確認
		if (!isValid(errorMessages, user, account, password)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("preAccount", account);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		HttpSession session = request.getSession();
		session.setAttribute("loginUser", user);
		session.removeAttribute("errorMessages");

		response.sendRedirect("./");

	}

	private boolean isValid(List<String> errorMessages, User user, String account, String password) {

		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");
		}

		if (StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");
		}

		if (errorMessages.size() != 0) {
			errorMessages.add("ログインに失敗しました");
			return false;
		}

		//アカウント停止状態の場合はNULLで帰ってくる：DAOの条件文にis_stopped = 0 を追加しているから
		if (user == null) {
			errorMessages.add("アカウントまたはパスワードが誤っています");
		}

		if (errorMessages.size() != 0) {
			errorMessages.add("ログインに失敗しました");
			return false;
		}

		return true;

	}

}

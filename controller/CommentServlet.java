package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Comment;
import bbs.beans.User;
import bbs.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		String cmt = request.getParameter("comment");

		//sessionをつかう  下みたいに
		//	User user = (User) session.getAttribute("messages");
		//		comment.setUserId(user.getId());
		if (!isValid(cmt, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			//request.getRequestDispatcher("./").forward(request, response);
			response.sendRedirect("./");
			return;
		}

		Comment comment = new Comment();
		comment.setText(cmt);

		//commentIdをセット Hiddenで送られている
		String messageId = request.getParameter("messageId");
		comment.setMessageId(Integer.parseInt(messageId));
		//UserIdをセット
		User user = (User) session.getAttribute("loginUser");
		comment.setUserId(user.getId());

		new CommentService().insert(comment);
		response.sendRedirect("./");

	}

	private boolean isValid(String comment, List<String> errorMessages) {

		if (StringUtils.isEmpty(comment)) {
			errorMessages.add("コメントを入力してください");
		} else if (comment.matches("^[  |　]+$")) {
			errorMessages.add("コメントを入力してください");
		} else if (500 < comment.length()) {

			errorMessages.add("500文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
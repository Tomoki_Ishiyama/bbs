
package bbs.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.service.CommentService;

@WebServlet(urlPatterns = { "/deleteComment" })
public class DeleteCommentServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		int deletedId = Integer.parseInt(request.getParameter("deletedId"));

		//	String deletedId = request.getParameter("deletedId");

		//message.setDeletedId(Integer.parseInt(deletedId));
		new CommentService().delete(deletedId);
		response.sendRedirect("./");
	}

}
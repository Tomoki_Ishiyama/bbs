package bbs.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//userIdをJSPから受け取る
		int userId = Integer.parseInt(request.getParameter("userId"));
		int isStopped = Integer.parseInt(request.getParameter("isStopped"));

		new UserService().stopOrContinue(userId, isStopped);

		response.sendRedirect("./management");
	}

}

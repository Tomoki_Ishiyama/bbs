package bbs.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletResponse httpResponse = (HttpServletResponse) response;
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpSession session = httpRequest.getSession();

		User user = (User) session.getAttribute("loginUser");
		String servletPath = httpRequest.getServletPath();
		String errorMessage = "ログインしてください。";

		//エラーにしたいことを先にかく
		//ユーザーがログインしてない　かつ　ログイン画面じゃないとき
		if (user == null && !servletPath.equals("/login")) {
			session.setAttribute("errorMessages", errorMessage);
			httpResponse.sendRedirect("./login");
		} else {//Userがあります
			chain.doFilter(request, response);
		}

	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}

}
